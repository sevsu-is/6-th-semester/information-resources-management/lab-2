#!/usr/bin/python

from core.avl_tree import AVLTree, Node
from typing import List
from core.perf_tools import get_performance
import people.tree_helper as tree_helper
import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plt
from people.people import Person

records: List[str] = tree_helper.read_records_from_csv('people.csv')

def fill_tree_function(*args) -> None:
    tree_helper.fill_tree_with_records(args[0][0], args[0][1][0])


def find_element_function(*args):
    tree: AVLTree = args[0][1][0]
    element_key: str = args[0][1][1]

    foundElement = tree.find(element_key)
    print(foundElement)

    return foundElement

def remove_element_function(*args):
    tree: AVLTree = args[0][1][0]
    element_key: str = args[0][1][1]
    tree.delete(element_key)


def measure_insertion_performance(records: List[str]) -> (List[int], List[float]):
    number_of_elements: List[int] = [1, 10, 100, 1_000, 10_000]
    elapsed_time: List[float] = []

    for measurement in number_of_elements:
        elapsed_time.append(get_performance(fill_tree_function, records[:measurement]))
    
    return number_of_elements, elapsed_time


def measure_search_performance(records: List[str], key: str) -> (List[int], List[float]):
    trees: List[AVLTree] = []
    number_of_elements: List[int] = [10, 100, 1_000, 10_000]
    elapsed_time: List[float] = []

    for measurement in number_of_elements:
        tree: AVLTree = AVLTree()
        tree_helper.fill_tree_with_records(tree, records[:measurement])
        trees.append(tree)

    for tree in trees:
        elapsed_time.append(get_performance(find_element_function, tree, key))
    
    return number_of_elements, elapsed_time


def measure_delete_performance(records: List[str], key: str) -> (List[int], List[float]):
    trees: List[AVLTree] = []
    number_of_elements: List[int] = [10, 100, 1_000, 10_000]
    elapsed_time: List[float] = []

    for measurement in number_of_elements:
        tree: AVLTree = AVLTree()
        tree_helper.fill_tree_with_records(tree, records[:measurement])
        trees.append(tree)

    for tree in trees:
        elapsed_time.append(get_performance(remove_element_function, tree, key))
    
    return number_of_elements, elapsed_time    


def on_measure_insertion_perf_btn_click():
    number_of_elements, elapsed_time = measure_insertion_performance(records)
    
    plt.plot(number_of_elements, elapsed_time, label='Insertion performance')
    plt.xlabel('Number of elements')
    plt.ylabel('Time taken')
    plt.show()


def on_measure_find_perf_btn_click(key: str):
    number_of_elements, elapsed_time = measure_search_performance(records, key)
    
    plt.plot(number_of_elements, elapsed_time, label='Search performance')
    plt.xlabel('Number of elements')
    plt.ylabel('Time taken to find')
    plt.show()


def on_measure_del_btn_click(tree: AVLTree, key: str):
    tree.delete(key)


def on_measure_del_perf_btn_click(key: str):
    number_of_elements, elapsed_time = measure_delete_performance(records, key)
    
    plt.plot(number_of_elements, elapsed_time, label='Delete performance')
    plt.xlabel('Number of elements')
    plt.ylabel('Time taken to delete')
    plt.show()


def _display_tree(treeView:ttk.Treeview, node: Node, treeViewNode = ''):
    if node == None:
        return
    
    currentNode = treeView.insert(treeViewNode, 'end', text=node.key, values=(node.data.name, node.data.email, node.data.profession))
    _display_tree(treeView, node.left, currentNode)
    _display_tree(treeView, node.right, currentNode)


def on_display_tree_btn_click(tree: AVLTree):
    treeWindow = tk.Tk()
    treeWindow.geometry('800x600')
    treeView = ttk.Treeview(treeWindow, height=50)
    treeView['columns'] = ('name', 'email', 'profession')

    treeView.heading('#0', text='ID')
    treeView.heading('name', text='Name')
    treeView.heading('email', text='Email')
    treeView.heading('profession', text='Profession')

    _display_tree(treeView, tree.root)

    treeView.pack(fill='both', expand=True)



def on_record_insert(name: str, email: str, profession: str, tree: AVLTree):
    tree.insert(name, Person(name, email, profession))


def on_add_record_btn_click(tree: AVLTree):
    addRecordWindow = tk.Tk()
    addRecordWindow.geometry('400x300')
    addRecordWindow.title("Manually adding record")

    nameLabel = tk.Label(addRecordWindow, text='Name')
    nameLabel.pack()
    nameField = tk.Entry(addRecordWindow)
    nameField.pack()

    emailLabel = tk.Label(addRecordWindow, text='Email')
    emailLabel.pack()
    emailField = tk.Entry(addRecordWindow)
    emailField.pack()

    professionLabel = tk.Label(addRecordWindow, text='Profession')
    professionLabel.pack()
    professionField = tk.Entry(addRecordWindow)
    professionField.pack()

    addBtn = tk.Button(addRecordWindow, text='Add record', 
        command=lambda:on_record_insert(nameField.get(), emailField.get(), professionField.get(), tree))
    addBtn.pack()


def main() -> None:
    tree = AVLTree()

    window = tk.Tk()
    window.geometry('450x300')
    window.title("AVL Tree Inspector")
    label = tk.Label(
        window, 
        text="Welcome to AVL Tree inspector! This program allows" + 
        " you to test and see " + 
        "the performance benefits of using AVL Trees for yourself",
        wraplength=350
    )
    label.grid(column=0, row=0, columnspan=2, padx=5, pady=2)

    insTestBtn = tk.Button(window, text='Batch insertion test', command=on_measure_insertion_perf_btn_click, width=15)
    insTestBtn.grid(column=0, row=1, columnspan=2, sticky='W', padx=5, pady=2)

    displayTreeBtn = tk.Button(window, text='Display tree', command=lambda:on_display_tree_btn_click(tree), width=15)
    displayTreeBtn.grid(column=0, row=2, columnspan=2, sticky='W', padx=5, pady=2)


    searchField = tk.Entry(window, width=35)
    searchField.grid(row=3, column=0, sticky='W', padx=2, pady=2)
    searchField.insert(0, 'Johna Alice')

    searchTestBtn = tk.Button(window, text='Search test', command=lambda:on_measure_find_perf_btn_click(searchField.get()),  width=15)
    searchTestBtn.grid(row=3, column = 1, sticky='W')

    deleteField = tk.Entry(window, width=35)
    deleteField.grid(row=4, column=0, sticky='W', padx=2, pady=2)
    deleteField.insert(0, 'Lenna Lowry')

    deleteButton = tk.Button(window, text='Delete', command=lambda:on_measure_del_btn_click(tree, deleteField.get()),  width=15)
    deleteButton.grid(row=4, column = 1, sticky='W')

    deleteTestBtn = tk.Button(window, text='Delete test', command=lambda:on_measure_del_perf_btn_click(deleteField.get()), width=15)
    deleteTestBtn.grid(row=5, column=0, sticky='W', padx=5, pady=2)

    addRecordBtn = tk.Button(window, text='Add record', command=lambda:on_add_record_btn_click(tree), width=15)
    addRecordBtn.grid(row=5, column=1, sticky='W', padx=5, pady=2)

    window.mainloop()


if __name__ == '__main__':
    main()
