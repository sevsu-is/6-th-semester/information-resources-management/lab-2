from typing import Generic, TypeVar

T = TypeVar('T')
class Person(Generic[T]):
    def __init__(self: T, name: str, email: str, profession: str):
        self.name = name
        self.email = email
        self.profession = profession