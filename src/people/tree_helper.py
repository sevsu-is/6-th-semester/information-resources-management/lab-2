from typing import List
from people.people import Person
from core.avl_tree import AVLTree
import csv


def read_records_from_csv(filename: str, limit: int = -1) -> List[str]:
    lines: List[str] = []
    
    with open(filename, 'r') as file:
        csv_reader = csv.reader(file)
        next(csv_reader)

        for i, row in enumerate(csv_reader):
            lines.append(row)

    return lines
                

def fill_tree_with_records(tree: AVLTree, records) -> None:
    for record in records:
        name, email, profession = record
        tree.insert(name, Person(name, email, profession))