import time
from statistics import geometric_mean
from core.avl_tree import AVLTree

NUMBER_OF_TRIALS = 10

def _do_performance_trial(function, *args) -> float:
    start_time = time.perf_counter()
    function(args)
    end_time = time.perf_counter()

    return end_time - start_time


def get_performance(function, *args) -> float:
    time_measurements: List[float] = []

    for i in range(NUMBER_OF_TRIALS):
        tree = AVLTree()
        time_taken = _do_performance_trial(function, tree, args)
        time_measurements.append(time_taken)

    return geometric_mean(time_measurements)