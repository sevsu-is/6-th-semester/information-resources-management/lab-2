from typing import TypeVar, Generic, List

T = TypeVar('T')
class Node(Generic[T]):
    def __init__(self: T, key, data):
        self.key = key
        self.data = data
        self.left = self.right = None
        self.height = 1


U = TypeVar('U')
class AVLTree(Generic[U]):
    def __init__(self: U):
        self.root = None
    
    def insert(self: U, key, data) -> None:
        self.root = self._insert(self.root, key, data)

    def _insert(self: U, node: Node, key, data) -> Node:
        if not node:
            return Node(key, data)
        elif key < node.key:
            node.left = self._insert(node.left, key, data)
        else:
            node.right = self._insert(node.right, key, data)

        node.height = 1 + max(self._get_height(node.left), self._get_height(node.right))
        balance: int = self._get_balance(node)

        if balance > 1 and key < node.left.key:
            return self._rotate_right(node)
        if balance < -1 and key > node.right.key:
            return self._rotate_left(node)
        if balance > 1 and key > node.left.key:
            node.left = self._rotate_left(node.left)
            return self._rotate_right(node)
        if balance < -1 and key < node.right.key:
            node.right = self._rotate_right(node.right)
            return self._rotate_left(node)
        
        return node

    
    def _get_height(self: U, node: Node) -> int:
        if not node:
            return 0
        return node.height
    

    def _get_balance(self: U, node: Node) ->  int:
        if not node:
            return 0
        return self._get_height(node.left) - self._get_height(node.right)
    

    def _rotate_right(self: U, z: Node):
        y: Node = z.left
        T3: Node = y.right

        # perform rotation
        y.right = z
        z.left = T3

        # update heights
        z.height = 1 + max(self._get_height(z.left), self._get_height(z.right))
        y.height = 1 + max(self._get_height(y.left), self._get_height(y.right))

        return y

    def _rotate_left(self: U, z: Node):
        y: Node = z.right
        T2: Node = y.left

        # perform rotation
        y.left = z
        z.right = T2

        # update heights
        z.height = 1 + max(self._get_height(z.left), self._get_height(z.right))
        y.height = 1 + max(self._get_height(y.left), self._get_height(y.right))

        return y

    def print(self: U) -> None:
        self._print(self.root, level = 0)

    def _print(self: U, node: Node, level: int) -> None:
        if node is not None:
            print('--' * level + str(node.key))
            for child in [node.left, node.right]:
                self._print(child, level + 1)

    def find(self: U, key):
        return self._find(self.root, key)
    
    def _find(self: U, node: Node, key):
        if node is None:
            return None

        if key > node.key:
            return self._find(node.right, key)
        elif key < node.key:
            return self._find(node.left, key)
        
        return node.data

    def delete(self: U, key) -> None:
        self.root = self._delete(self.root, key)

    def _delete(self: U, node: Node, key) -> Node:
        if not node:
            return node
        elif key < node.key:
            node.left = self._delete(node.left, key)
        elif key > node.key:
            node.right = self._delete(node.right, key)
        else:
            # node to be deleted found
            if not node.left and not node.right:
                # node has no children
                node = None
            elif not node.left:
                # node has only right child
                node = node.right
            elif not node.right:
                # node has only left child
                node = node.left
            else:
                # node has both left and right children
                # find the minimum value in the right subtree
                min_node = self._get_min(node.right)
                node.key = min_node.key
                node.data = min_node.data
                node.right = self._delete(node.right, min_node.key)

        # if the node was deleted, return None
        if not node:
            return node

        # update height and balance factor
        node.height = 1 + max(self._get_height(node.left), self._get_height(node.right))
        balance = self._get_balance(node)

        # re-balance the tree if necessary
        if balance > 1 and self._get_balance(node.left) >= 0:
            return self._rotate_right(node)
        if balance > 1 and self._get_balance(node.left) < 0:
            node.left = self._rotate_left(node.left)
            return self._rotate_right(node)
        if balance < -1 and self._get_balance(node.right) <= 0:
            return self._rotate_left(node)
        if balance < -1 and self._get_balance(node.right) > 0:
            node.right = self._rotate_right(node.right)
            return self._rotate_left(node)

        return node

    def _get_min(self: U, node: Node) -> Node:
        if not node.left:
            return node
        return self._get_min(node.left)